# Ob-Havo ma'lumotini olib beruvchi dastur

Bu dasturni topshiriq sifatida berilgan.

Topshiriq mazmuni:

Bitta microservice yarating qaysiki ob havo haqida ma’lumot to’playdigan bo’lsin va har 10 daqiqada ma’lumotlar avtomatik ravishda yangilanib turishi kerak. Misol uchun https://openweathermap.org/api shu saytning API dan foydalanishingiz mumkin lekin bu shart degani emas o’zingiz xohlagan servisdan ma’lumot to’plashingiz mumkin.

##Topshiriq Bajarildi!

Ushbu topshiriqni bajarishda https://openweathermap.org/api dan foydalanildi.

## Ishlatilgan

- Python 3.8
- PostgreSql 12.7
- Django 3.2.9

## Dasturni ishga tushirish
1. Git orqali o'zingizda yuklab oling
2. requirements.txt da joylashgan barcha kerakli modullarni o'rnatib oling
3. PostgreSql orqali yangi baza hosil qiling.
4. Loyihani ichida ``src/openweathermap`` shu bo'limga kirib ``.env`` fileni hosil qilib oling. uni ichida sozlamalar yoziladi. qanday sozlamalr bo'lishi esa ``.env.example`` file da joylashgan.
5. Bazaga kerakli migratsiyalarni o'rnatib olamiz. ``python manage.py migrate``
6. Hududlarni bazaga yuklab oling. ``python manage.py loaddata city_list.json``
7. Dasturni admin qismi uchun foydalanuvchi hosil qilib oling. ``python manage.py createsuperuser``
8. Web Serverni ishga tushish uchun ``python manage.py runserver``
9. Admin uchun URL ``http://127.0.0.1:8000/admin/``
10. Har 10 minutda ma'lumot olib kelish uchun Celery ni ishga tushirish kerak.
 
    ``celery -A openweathermap beat --loglevel=info``
    
    ``celery -A openweathermap worker --loglevel=info``

## API dan foydalanish
- API da Ob-havo ma'lumotlarini bilish methodi mavjud.
``http://127.0.0.1:8000/api/v1/weather/?city=Toshkent Shahri``

``city`` bu parametr orqali hudud nomi beriladi.
Mavjud hududlarni admin orqali City jdvalidan ko'rib olishingiz mumkin.

- ``period`` parametri bu 2 xil qiymatni qabul qiladi:
    
    - ``weekly`` 1 xaftalif ma'lumot olish
    - ``monthly`` 1 oylik ma'lumotni olish

Agar ``period`` parametr berilmasa bugungi kunlik ma'lumotlarni beradi.


## Qo'shimcha
Barcha ma'lumotlarni boshqarish, CRUD, export Adminka orqali amalga oshiriladi.



